# @author bfarnham
# @date 22-Sep-2016
# The purpose of this file is to set parameters for building the CAEN OPC-UA server using the 
# Open62541 backend. Open62541 is an open source OPC-UA toolkit (see open62541.org for details).

message("using build configuration open6_build.cmake")
message(STATUS "environment vars: BOOST_HOME [$ENV{BOOST_HOME}]")

#-------
#Boost
#-------
if( NOT DEFINED ENV{BOOST_HOME} )
	message(FATAL_ERROR "environment variable BOOST_HOME not defined - please set this to a 64bit boost installation")
else()	
	SET( BOOST_PATH_HEADERS $ENV{BOOST_HOME}/include )
	SET( BOOST_PATH_LIBS $ENV{BOOST_HOME}/lib )
endif()
message(STATUS "BOOST - include [${BOOST_PATH_HEADERS}] libs [${BOOST_PATH_LIBS}] BOOST_LIB_SUFFIX [$ENV{BOOST_LIB_SUFFIX}]")
 
if(NOT TARGET libboostprogramoptions)
        add_library(libboostprogramoptions STATIC IMPORTED)
        set_property(TARGET libboostprogramoptions PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_program_options$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostsystem)
        add_library(libboostsystem STATIC IMPORTED)
        set_property(TARGET libboostsystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_system$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostfilesystem)
        add_library(libboostfilesystem STATIC IMPORTED)
        set_property(TARGET libboostfilesystem PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_filesystem$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostchrono) 
        add_library(libboostchrono STATIC IMPORTED)
        set_property(TARGET libboostchrono PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_chrono$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostdatetime) 
        add_library(libboostdatetime STATIC IMPORTED)
        set_property(TARGET libboostdatetime PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_date_time$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostthread) 
        add_library(libboostthread STATIC IMPORTED)
        set_property(TARGET libboostthread PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_thread$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlog)
        add_library(libboostlog STATIC IMPORTED)
        set_property(TARGET libboostlog PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log$ENV{BOOST_LIB_SUFFIX}.a)
endif()
if(NOT TARGET libboostlogsetup)
        add_library(libboostlogsetup STATIC IMPORTED)
        set_property(TARGET libboostlogsetup PROPERTY IMPORTED_LOCATION ${BOOST_PATH_LIBS}/libboost_1_59_0_log_setup$ENV{BOOST_LIB_SUFFIX}.a)
endif()

set( BOOST_LIBS  libboostlogsetup libboostlog libboostsystem libboostfilesystem libboostthread libboostprogramoptions libboostchrono libboostdatetime -lrt)

#------
# OPCUA: builds using the Open62541 toolkit differ from Unified Auomation toolkit builds. Servers built with Unified Automation follow
# a fairly standard headers+binaries style builds, Servers built with Open62541 however are slightly more 'exotic'. The Open62541
# toolkit source code is obtained from github and built with the server. This particular bit of magic happens in the open62541-compat
# module; see open62541-compat/prepare.py for details.
#------
add_definitions( -DBACKEND_OPEN62541 )
remove_definitions( -DBACKEND_UATOOLKIT )
set( OPEN62541_DEPENDENCIES -pthread -lssl -lcrypto -lrt )
SET( OPCUA_TOOLKIT_PATH "" ) # open62541 toolkit is cloned from github and built via the open62541-compat module (see open62541-compat/prepare.py)
SET( OPCUA_TOOLKIT_LIBS_RELEASE "${PROJECT_SOURCE_DIR}/open62541-compat/open62541/build/libopen62541.a" ${OPEN62541_DEPENDENCIES})
SET( OPCUA_TOOLKIT_LIBS_DEBUG "${PROJECT_SOURCE_DIR}/open62541-compat/open62541/build/libopen62541.a" ${OPEN62541_DEPENDENCIES})

#-----
# LogIt
#-----
SET( LOGIT_HAS_STDOUTLOG TRUE )
SET( LOGIT_HAS_BOOSTLOG TRUE )
SET( LOGIT_HAS_UATRACE FALSE )
MESSAGE( STATUS "LogIt build options: stdout [${LOGIT_HAS_STDOUTLOG}] boost [${LOGIT_HAS_BOOSTLOG}] uaTrace [${LOGIT_HAS_UATRACE}]" )

#-----
# XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested
SET( XML_LIBS "-lxerces-c" )

#-----
# Compiler options (note: currently linux specific)
#-----
add_definitions(-Wall -Wno-deprecated -std=gnu++0x -Wno-literal-suffix)
