A simple, deterministic (ish) server for testing purposes. The server runs a
thread that on each cycle, increments all address space values. The server
also accepts address space writes. End user can switch on/off the increment
thread and set the increment thread cycle sleep from an OPC-UA client.

Note this server is designed to work with either open62541 or Unified
Automation backends (build instructions for both cases below).


To build, from clean, with open6

Include the open62541-compat module as an optional module (i.e. writes some 
stuff to FrameworkInternals/EnabledModule. Note that this selection is NOT 
persisted (to provide the flexibility of building with Open6 or Unified 
Automation). Note, the prepare steps are also required (to clone open6-compat
and to clone and build open6 itself)
- set the BOOST_HOME variable
- python quasar.py enable_module open62541-compat
- python quasar.py prepare_build 
- cd open62541-compat && python prepare.py && cd ..
- python quasar.py build Debug open62541_build.cmake 



To build, from clean, with Unified Automation
- set the BOOST_HOME variable
- set the UNIFIED_AUTOMATION_HOME variable
- python quasar.py build Debug enice_sl6_configuration.cmake

