
/*  © Copyright CERN, 2015. All rights not expressly granted are reserved.

    The stub of this file was generated by Quasar (additional info: using transform designToDeviceHeader.xslt)
    on 2016-10-31T17:31:46.783+01:00

    Quasar is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public Licence as published by
    the Free Software Foundation, either version 3 of the Licence.
    Quasar is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public Licence for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Quasar.  If not, see <http://www.gnu.org/licenses/>.



 */





#ifndef __DStringProperty__H__
#define __DStringProperty__H__

#include <vector>
#include <boost/thread/mutex.hpp>

#include <statuscode.h>
#include <uadatetime.h>
#include <session.h>


#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DStringProperty.h>


namespace Device
{




class
    DStringProperty
    : public Base_DStringProperty
{

public:
    /* sample constructor */
    explicit DStringProperty (
        const Configuration::StringProperty & config,
        Parent_DStringProperty * parent
    ) ;
    /* sample dtr */
    ~DStringProperty ();




    /* delegators for
    cachevariables and sourcevariables */

    /* Note: never directly call this function. */


    UaStatus writeStringValue ( const UaString & v);


private:
    /* Delete copy constructor and assignment operator */
    DStringProperty( const DStringProperty & );
    DStringProperty& operator=(const DStringProperty &other);

    // ----------------------------------------------------------------------- *
    // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
    // -     Don't change this comment, otherwise merge tool may be troubled.  *
    // ----------------------------------------------------------------------- *

public:
    void increment();
private:


};





}

#endif // include guard
